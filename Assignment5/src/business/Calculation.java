/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Individual;

/**
 *
 * @author schau
 */
public class Calculation {

    public int calculateMaleRiskScore(Individual individual) {

        int ageChoPoints = 0;
        int totalCHLPoints = 0;
        int hdlCHLPoints = 0;
        int systolicBPPoints = 0;
        int diabeticPoints = 0;
        int smokerPoints = 0;
        int totalPoints = 0;
        int riskScore = 0;

        if (individual.getAge() >= 30 && individual.getAge() <= 34) {
            ageChoPoints = - 1;

        } else if (individual.getAge() >= 35 && individual.getAge() <= 39) {
            ageChoPoints = 0;

        } else if (individual.getAge() >= 40 && individual.getAge() <= 44) {
            ageChoPoints = 1;

        } else if (individual.getAge() >= 45 && individual.getAge() <= 49) {
            ageChoPoints = 2;

        } else if (individual.getAge() >= 50 && individual.getAge() <= 54) {
            ageChoPoints = 3;

        } else if (individual.getAge() >= 55 && individual.getAge() <= 59) {
            ageChoPoints = 4;

        } else if (individual.getAge() >= 60 && individual.getAge() <= 64) {
            ageChoPoints = 5;

        } else if (individual.getAge() >= 65 && individual.getAge() <= 69) {
            ageChoPoints = 6;

        } else if (individual.getAge() >= 70 && individual.getAge() <= 74) {
            ageChoPoints = 7;

        }

        System.out.println("Points based on Age: " + ageChoPoints);

        if (individual.getTotalCholesterol() < 4.14) {
            totalCHLPoints = - 3;
        }
        if (individual.getTotalCholesterol() <= 5.17 && individual.getTotalCholesterol() >= 4.15) {
            totalCHLPoints = 0;
        }
        if (individual.getTotalCholesterol() >= 5.18 && individual.getTotalCholesterol() <= 6.21) {
            totalCHLPoints = 1;
        }
        if (individual.getTotalCholesterol() >= 6.22 && individual.getTotalCholesterol() <= 7.24) {
            totalCHLPoints = 2;
        }
        if (individual.getTotalCholesterol() >= 7.25) {
            totalCHLPoints = 3;
        }

        System.out.println("Points based on Total Cholesterol: " + totalCHLPoints);

        if (individual.getHdlCholesterol() < 0.9) {
            hdlCHLPoints = 2;
        }

        if (individual.getHdlCholesterol() >= 0.91 && individual.getHdlCholesterol() <= 1.16) {
            hdlCHLPoints = 1;
        }
        if (individual.getHdlCholesterol() >= 1.17 && individual.getHdlCholesterol() <= 1.29) {
            hdlCHLPoints = 0;
        }
        if (individual.getHdlCholesterol() >= 1.30 && individual.getHdlCholesterol() <= 1.55) {
            hdlCHLPoints = 0;
        }
        if (individual.getHdlCholesterol() >= 1.56) {
            hdlCHLPoints = -2;
        }

        System.out.println("Points based on HDL: " + hdlCHLPoints);

        if (individual.getSystolicBP() < 120) {
            systolicBPPoints = 0;
        }
        if (individual.getSystolicBP() >= 120 && individual.getSystolicBP() <= 129) {
            systolicBPPoints = 0;
        }
        if (individual.getSystolicBP() >= 130 && individual.getSystolicBP() <= 139) {
            systolicBPPoints = 1;
        }
        if (individual.getSystolicBP() >= 140 && individual.getSystolicBP() <= 149) {
            systolicBPPoints = 2;
        }
        if (individual.getSystolicBP() >= 160) {
            systolicBPPoints = 3;
        }
        System.out.println("Points based on Systolic BP: " + systolicBPPoints);

        if (individual.getIsDiabetic().equalsIgnoreCase("YES")) {
            diabeticPoints = 2;
        }
        if (individual.getIsDiabetic().equalsIgnoreCase("NO")) {
            diabeticPoints = 0;
        }
        System.out.println("Points based on Diabetics: " + diabeticPoints);

        if (individual.getIsSmoker().equals("YES")) {
            smokerPoints = 2;
        }
        if (individual.getIsSmoker().equals("NO")) {
            smokerPoints = 0;
        }
        System.out.println(" Points based on smoking habbit :" + smokerPoints);

        // Calculating Total Points
        totalPoints = ageChoPoints + totalCHLPoints + hdlCHLPoints + smokerPoints + diabeticPoints + systolicBPPoints;

        System.out.println("TOTAL CHL POINTS: " + totalPoints);

        //Determine CHD risk from point total
        if (totalPoints < -1) {
            riskScore = 2;
        }
        if (totalPoints == 0) {
            riskScore = riskScore + 3;
        }
        if (totalPoints == 1) {
            riskScore = 3;
        }
        if (totalPoints == 2) {
            riskScore = 4;
        }
        if (totalPoints == 3) {
            riskScore = 5;
        }
        if (totalPoints == 4) {
            riskScore = 7;
        }
        if (totalPoints == 5) {
            riskScore = 8;
        }
        if (totalPoints == 6) {
            riskScore = 10;
        }
        if (totalPoints == 7) {
            riskScore = 13;
        }
        if (totalPoints == 8) {
            riskScore = 16;
        }
        if (totalPoints == 9) {
            riskScore = 20;
        }
        if (totalPoints == 10) {
            riskScore = 25;
        }
        if (totalPoints == 11) {
            riskScore = 31;
        }
        if (totalPoints == 12) {
            riskScore = 37;
        }
        if (totalPoints == 13) {
            riskScore = 45;
        }
        if (totalPoints >= 14) {
            riskScore = 53;
        }

        return riskScore;
    }

    public int calculateFemaleRiskScore(Individual individual) {
        int ageChoPoints = 0;
        int totalCHLPoints = 0;
        int hdlCHLPoints = 0;
        int systolicBPPoints = 0;
        int diabeticPoints = 0;
        int smokerPoints = 0;
        int totalPoints = 0;
        int riskScore = 0;

        if (individual.getAge() >= 30 && individual.getAge() <= 34) {
            ageChoPoints = - 9;

        } else if (individual.getAge() >= 35 && individual.getAge() <= 39) {
            ageChoPoints = - 4;
        } else if (individual.getAge() >= 40 && individual.getAge() <= 44) {
            ageChoPoints = 0;
        } else if (individual.getAge() >= 45 && individual.getAge() <= 49) {
            ageChoPoints = 3;
        } else if (individual.getAge() >= 50 && individual.getAge() <= 54) {
            ageChoPoints = 6;
        } else if (individual.getAge() >= 55 && individual.getAge() <= 59) {
            ageChoPoints = 7;
        } else if (individual.getAge() >= 60 && individual.getAge() <= 64) {
            ageChoPoints = 8;
        } else if (individual.getAge() >= 65 && individual.getAge() <= 69) {
            ageChoPoints = 8;
        } else if (individual.getAge() >= 70 && individual.getAge() <= 74) {
            ageChoPoints = 8;
        }

        System.out.println("Points based on Age: " + ageChoPoints);

        if (individual.getTotalCholesterol() < 4.14) {
            totalCHLPoints = - 2;
        }
        if (individual.getTotalCholesterol() <= 5.17 && individual.getTotalCholesterol() >= 4.15) {
            totalCHLPoints = 0;
        }
        if (individual.getTotalCholesterol() >= 5.18 && individual.getTotalCholesterol() <= 6.21) {
            totalCHLPoints = 1;
        }
        if (individual.getTotalCholesterol() >= 6.22 && individual.getTotalCholesterol() <= 7.24) {
            totalCHLPoints = 1;
        }
        if (individual.getTotalCholesterol() >= 7.25) {
            totalCHLPoints = 3;
        }

        System.out.println("Points based on Total Cholesterol: " + totalCHLPoints);

        if (individual.getHdlCholesterol() < 0.9) {
            hdlCHLPoints = 5;
        }

        if (individual.getHdlCholesterol() >= 0.91 && individual.getHdlCholesterol() <= 1.16) {
            hdlCHLPoints = 2;
        }
        if (individual.getHdlCholesterol() >= 1.17 && individual.getHdlCholesterol() <= 1.29) {
            hdlCHLPoints = 1;
        }
        if (individual.getHdlCholesterol() >= 1.30 && individual.getHdlCholesterol() <= 1.55) {
            hdlCHLPoints = 0;
        }
        if (individual.getHdlCholesterol() >= 1.56) {
            hdlCHLPoints = - 3;
        }

        System.out.println("Points based on HDL Cholesterol: " + hdlCHLPoints);

        if (individual.getSystolicBP() < 120) {
            systolicBPPoints = - 3;
        }
        if (individual.getSystolicBP() >= 120 && individual.getSystolicBP() <= 129) {
            systolicBPPoints = 0;
        }
        if (individual.getSystolicBP() >= 130 && individual.getSystolicBP() <= 139) {
            systolicBPPoints = 0;
        }
        if (individual.getSystolicBP() >= 140 && individual.getSystolicBP() <= 149) {
            systolicBPPoints = 2;
        }
        if (individual.getSystolicBP() >= 160) {
            systolicBPPoints = 3;
        }

        System.out.println("Points based on Systolic BP: " + systolicBPPoints);

        if (individual.getIsDiabetic().equals("YES")) {
            diabeticPoints = 4;
        }
        if (individual.getIsDiabetic().equals("NO")) {
            diabeticPoints = 0;
        }

        System.out.println("Points based on Diabetics: " + diabeticPoints);

        if (individual.getIsSmoker().equals("YES")) {
            smokerPoints = 2;
        }
        if (individual.getIsSmoker().equals("NO")) {
            smokerPoints = 0;
        }

        System.out.println("Points based on Smoking Habit: " + smokerPoints);

        // Calculating Total Points
        totalPoints = ageChoPoints + totalCHLPoints + hdlCHLPoints + smokerPoints + diabeticPoints + systolicBPPoints;

        System.out.println("TOTAL POINTS:" + totalPoints);

        //Determine CHD risk from point total
        if (totalPoints < -2) {
            riskScore = 1;
        }
        if (totalPoints == -1) {
            riskScore = 2;
        }
        if (totalPoints == 0) {
            riskScore = 2;
        }
        if (totalPoints == 1) {
            riskScore = 2;
        }
        if (totalPoints == 2) {
            riskScore = 3;
        }
        if (totalPoints == 3) {
            riskScore = 3;
        }
        if (totalPoints == 4) {
            riskScore = 4;
        }
        if (totalPoints == 5) {
            riskScore = 4;
        }
        if (totalPoints == 6) {
            riskScore = 5;
        }
        if (totalPoints == 7) {
            riskScore = 6;
        }
        if (totalPoints == 8) {
            riskScore = 7;
        }
        if (totalPoints == 9) {
            riskScore = 8;
        }
        if (totalPoints == 10) {
            riskScore = 10;
        }
        if (totalPoints == 11) {
            riskScore = 11;
        }
        if (totalPoints == 12) {
            riskScore = 13;
        }
        if (totalPoints == 13) {
            riskScore = 15;
        }
        if (totalPoints == 14) {
            riskScore = 53;
        }
         if (totalPoints ==15) {
            riskScore = 18;
        }
          if (totalPoints == 16) {
            riskScore = 24;
        }
           if (totalPoints >=17) {
            riskScore = 27;
        }
        return riskScore;
    }

}
