/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author schau
 */
public class VitalSigns {
    
    private int totalCholestrol;
    private int hdlCholestrol;
    private boolean isSmoker;
    private boolean isDiabetic;
    private int bloodPressure;
    private boolean isTreatedForHighBp;
    private int systolicBloodPressure;
    private int diastolicBloodPressure;
    private int respirationRate;
    private int pulseRate;
    private Date vitalSignRecordedDate;

    public int getTotalCholestrol() {
        return totalCholestrol;
    }

    public void setTotalCholestrol(int totalCholestrol) {
        this.totalCholestrol = totalCholestrol;
    }

    public int getHdlCholestrol() {
        return hdlCholestrol;
    }

    public void setHdlCholestrol(int hdlCholestrol) {
        this.hdlCholestrol = hdlCholestrol;
    }

    public boolean isIsSmoker() {
        return isSmoker;
    }

    public void setIsSmoker(boolean isSmoker) {
        this.isSmoker = isSmoker;
    }

    public boolean isIsDiabetic() {
        return isDiabetic;
    }

    public void setIsDiabetic(boolean isDiabetic) {
        this.isDiabetic = isDiabetic;
    }

    public int getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(int bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public boolean isIsTreatedForHighBp() {
        return isTreatedForHighBp;
    }

    public void setIsTreatedForHighBp(boolean isTreatedForHighBp) {
        this.isTreatedForHighBp = isTreatedForHighBp;
    }

    public int getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(int systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    public int getDiastolicBloodPressure() {
        return diastolicBloodPressure;
    }

    public void setDiastolicBloodPressure(int diastolicBloodPressure) {
        this.diastolicBloodPressure = diastolicBloodPressure;
    }

    public int getRespirationRate() {
        return respirationRate;
    }

    public void setRespirationRate(int respirationRate) {
        this.respirationRate = respirationRate;
    }

    public int getPulseRate() {
        return pulseRate;
    }

    public void setPulseRate(int pulseRate) {
        this.pulseRate = pulseRate;
    }

    public Date getVitalSignRecordedDate() {
        return vitalSignRecordedDate;
    }

    public void setVitalSignRecordedDate(Date vitalSignRecordedDate) {
        this.vitalSignRecordedDate = vitalSignRecordedDate;
    }

    @Override
    public String toString() {
        return "VitalSigns{" + "totalCholestrol=" + totalCholestrol + ", hdlCholestrol=" + hdlCholestrol + ", isSmoker=" + isSmoker + ", isDiabetic=" + isDiabetic + ", bloodPressure=" + bloodPressure + ", isTreatedForHighBp=" + isTreatedForHighBp + ", systolicBloodPressure=" + systolicBloodPressure + ", diastolicBloodPressure=" + diastolicBloodPressure + ", respirationRate=" + respirationRate + ", pulseRate=" + pulseRate + ", vitalSignRecordedDate=" + vitalSignRecordedDate + '}';
    }
    
}
