/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Directory.CommunityDirectory;
import Directory.FamilyDirectory;
import Directory.HouseDirectory;
import Directory.PersonDirectory;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.fluttercode.datafactory.impl.DataFactory;

/**
 *
 * @author Oza Sagar
 */
public class Initialization {

    public int personIdCount = 1;
    public int familyIdCount = 1;
    public int houseIdCount = 1;
    public ReportCalculations reportCalculations;
    PersonDirectory personDirectory;
    FamilyDirectory familyDirectory;
    HouseDirectory houseDirectory;
    CommunityDirectory communityDirectory;
    Community community = new Community();
    City city;

    public Initialization() {
        personDirectory = new PersonDirectory();
        familyDirectory = new FamilyDirectory();
        houseDirectory = new HouseDirectory();
        communityDirectory = new CommunityDirectory();
        city = new City();
        reportCalculations = new ReportCalculations();
    }

    //Sagar Oza-- Initializing values of family Members
    public HashMap<String, Object> initializePersonDirectory() {
        DataFactory dataFactory = new DataFactory();
        String[] genderValues = {"Male", "Female"};
        ArrayList<Person> personDirectoryList = new ArrayList<Person>();
        personDirectoryList = personDirectory.getPersonDirectory();
     //   System.out.println("Size of perosn directory list" + personDirectoryList.size());
        while (personDirectoryList.size() < 1002) {
            if (personDirectory.getPersonDirectory().size() == 1000) {
             //   System.out.println("Final Size: " + personDirectory.getPersonDirectory().size());
                break;
            }
            int familiesInHouse = dataFactory.getNumberBetween(1, 4);
//gets size of family between 1 and 3 in a house. Maximum would be 2 and Minimum would be 1 family in the house
         //   System.out.println("Families in House " + houseIdCount + " : " + familiesInHouse);

            House house = new House();
            house.setHouseName(dataFactory.getLastName() + "'s House");
            house.setHouseId(houseIdCount);
            house.setNumberOfFamily(familiesInHouse);
            house.setCommunityId(1);
            ArrayList<Family> familyListForOneHouse = new ArrayList<Family>();
            for (int i = 1; i <= familiesInHouse; i++) //runs for number Of Families In House times.eg : if 2 families in a house than 2 times
            {
                int totalFamilyMembersInFamily = dataFactory.getNumberBetween(1, 5);
                //System.out.println("Total members in Family: " + totalFamilyMembersInFamily);
                Family family = new Family();
                family.setFamilyId(familyIdCount);
                family.setFamilyName(dataFactory.getLastName() + "'s family");
                family.setTotalNumberOfMembersInFamily(totalFamilyMembersInFamily);
                family.setHouseId(houseIdCount);
                ArrayList personInFamilyList = new ArrayList();

                String addressFoFamilyMembers = " Apartment No: " + dataFactory.getNumberText(2) + ", " + dataFactory.getAddress() + ", " + dataFactory.getCity();

                for (int j = 1; j <= totalFamilyMembersInFamily; j++) {
                    int personAge = dataFactory.getNumberBetween(20, 95);
                    if ((personAge > 20) && (personAge < 30)) {
                        Adult adult = new Adult();
                        adult.setPersonId(personIdCount);
                        adult.setFirstName(dataFactory.getFirstName());
                        adult.setLastName(dataFactory.getLastName());
                        adult.setAge(personAge);
                        adult.setGender(dataFactory.getItem(genderValues, 50, "Male"));
                        adult.setEmailId(adult.getLastName() + "." + adult.getFirstName() + "@husky.neu.edu");
                        adult.setPersonType(adult.getPersonType());
                        adult.setCity("Boston");
                        adult.setAddress(addressFoFamilyMembers);
                        adult.setFamilyId(familyIdCount);
                        if (adult.getGender().equalsIgnoreCase("Male")) {
                            adult.setVitalSignHistory(getVitalSignsRecordsForMen(personIdCount));
                        } else {
                            adult.setVitalSignHistory(getVitalSignsRecordsForWomen(personIdCount));
                        }

                      //  System.out.println("Person: " + adult);
                        personInFamilyList.add(adult);
                        personDirectoryList.add(adult);
                        if (personDirectoryList.size() == 1000) {
                            break;
                        }

                    } else if ((personAge > 30) && (personAge < 60)) {
                        Parent parent = new Parent();
                        parent.setPersonId(personIdCount);
                        parent.setFirstName(dataFactory.getFirstName());
                        parent.setLastName(dataFactory.getLastName());
                        parent.setAge(personAge);
                        parent.setGender(dataFactory.getItem(genderValues, 50, "Male"));
                        parent.setEmailId(parent.getLastName() + "." + parent.getFirstName() + "@husky.neu.edu");
                        parent.setPersonType(parent.getPersonType());
                        parent.setCity("Boston");
                        parent.setAddress(addressFoFamilyMembers);
                        parent.setFamilyId(familyIdCount);
                        if (parent.getGender().equalsIgnoreCase("Male")) {
                            parent.setVitalSignHistory(getVitalSignsRecordsForMen(personIdCount));
                        } else {
                            parent.setVitalSignHistory(getVitalSignsRecordsForWomen(personIdCount));
                        }
                       // System.out.println("Person: " + parent);
                        if (parent.getGender().equalsIgnoreCase("Male")) {

                            parent.setRiskScore(reportCalculations.calculateMaleRiskScore(parent.getVitalSignHistory().getVitalSignHistory().get(0), parent.getAge()));

                        }
                        if (parent.getGender().equalsIgnoreCase("Female")) {

                            parent.setRiskScore(reportCalculations.calculateFemaleRiskScore(parent.getVitalSignHistory().getVitalSignHistory().get(0), parent.getAge()));

                        }

                        personInFamilyList.add(parent);
                        personDirectoryList.add(parent);
                        if (personDirectoryList.size() == 1000) {
                            break;
                        }

                    } else if (personAge > 65) {
                        GrandParent grandParent = new GrandParent();
                        grandParent.setPersonId(personIdCount);
                        grandParent.setFirstName(dataFactory.getFirstName());
                        grandParent.setLastName(dataFactory.getLastName());
                        grandParent.setAge(personAge);
                        grandParent.setGender(dataFactory.getItem(genderValues, 50, "Male"));
                        grandParent.setEmailId(grandParent.getLastName() + "." + grandParent.getFirstName() + "@husky.neu.edu");
                        grandParent.setPersonType(grandParent.getPersonType());
                        grandParent.setCity("Boston");
                        grandParent.setAddress(addressFoFamilyMembers);
                        grandParent.setFamilyId(familyIdCount);
                        if (grandParent.getGender().equalsIgnoreCase("Male")) {
                            grandParent.setVitalSignHistory(getVitalSignsRecordsForMen(personIdCount));
                        } else {
                            grandParent.setVitalSignHistory(getVitalSignsRecordsForWomen(personIdCount));
                        }
                     //   System.out.println("Person: " + grandParent);

                        if (grandParent.getGender().equalsIgnoreCase("Male")) {

                            grandParent.setRiskScore(reportCalculations.calculateMaleRiskScore(grandParent.getVitalSignHistory().getVitalSignHistory().get(0), grandParent.getAge()));
                        }

                        if (grandParent.getGender().equalsIgnoreCase("Female")) {

                            grandParent.setRiskScore(reportCalculations.calculateFemaleRiskScore(grandParent.getVitalSignHistory().getVitalSignHistory().get(0), grandParent.getAge()));
                        }
                        personInFamilyList.add(grandParent);
                        personDirectoryList.add(grandParent);
                        if (personDirectoryList.size() == 1000) {
                            break;
                        }
                    }
                    //System.out.println("Person count before ++ : " + personIdCount);
                    personIdCount++;
                    if (personIdCount == 1000) {
                        break;
                    }
                    //incrementing person count after every iteration of person type before the end of inner for loop               
                    //System.out.println("Person count after ++ : "  + personIdCount);
                }                     //end of totalFamilyMembersInFamily loop

                familyIdCount++;     //incementing family count or ID of a family before the end of outer loop. after this new family ID will be created
                family.setPersonInFamilyList(personInFamilyList);
                familyListForOneHouse.add(family);
                familyDirectory.getFamilyList().add(family);

            }                         //end of familiesInHouseSize loop

            houseIdCount++;            //incrementing house ID before the end of while loop after this new house ID will be created.
            house.setFamilyList(familyListForOneHouse);
            houseDirectory.getHouseDirectory().add(house);
            //System.out.println("Person count: " + personDirectory.getPersonDirectory().size() + "Person count: " + personIdCount); 
            if (personIdCount == 1000) {
                break;
            }
        }

        //System.out.println("Person count: " + personIdCount);
        //System.out.println("Person Directory: " + personDirectory);
        //System.out.println("Family Directory: " + familyDirectory);
//        for(int i=0; i < 10; i++)
//        {
//            List<House> listOfHouse = houseDirectory.getHouseDirectory().subList(0, houseDirectory.getHouseDirectory().size()/10);
//            
//        }
      //  System.out.println("House Directory: " + houseDirectory.getHouseDirectory().size());
        community.setHouseDirectory(houseDirectory);
        communityDirectory.getCommunityList().add(community);
        city.setCommunityDirectory(communityDirectory);
        city.setFamilyDirectory(familyDirectory);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("city", city);
        hashMap.put("PersonDirectory", personDirectory);
        return hashMap;
    }

    private VitalSignHistory getVitalSignsRecordsForMen(int personIdCount1) {
        DataFactory dataFactory = new DataFactory();
        VitalSignHistory vsh = new VitalSignHistory();
        for (int i = 0; i < 5; i++) {
            VitalSigns vitalSigns = new VitalSigns();
            vitalSigns.setTotalCholestrol(dataFactory.getNumberBetween(130, 330));
            vitalSigns.setHdlCholestrol(dataFactory.getNumberBetween(25, 75));
            if (dataFactory.getNumberBetween(1, 3) == 1) //means 1 means diabetic
            {
                vitalSigns.setIsDiabetic(true);
            } else {
                vitalSigns.setIsDiabetic(false);
            }
            int smokingNumber = dataFactory.getNumberBetween(1, 3);
            if (smokingNumber == 1) //means 1 means smoer
            {
               // System.out.println("-----------Smoking set to true-------" + smokingNumber);
                vitalSigns.setIsSmoker(true);
            } else {
               // System.out.println("-----------Smoking set to false-------" + smokingNumber);
                vitalSigns.setIsSmoker(false);
            }
            vitalSigns.setSystolicBloodPressure(dataFactory.getNumberBetween(100, 200));
            vitalSigns.setDiastolicBloodPressure(dataFactory.getNumberBetween(70, 120));
            Date minDate = dataFactory.getDate(2000, 1, 1);
            Date maxDate = new Date();
            vitalSigns.setVitalSignRecordedDate(dataFactory.getDateBetween(maxDate, minDate));
            vsh.getVitalSignHistory().add(vitalSigns);
            //System.out.println("Vita Sign: " + vitalSigns);
        }
        return vsh;
    }

    private VitalSignHistory getVitalSignsRecordsForWomen(int personIdCount1) {
        DataFactory dataFactory = new DataFactory();
        VitalSignHistory vsh = new VitalSignHistory();
        for (int i = 0; i < 5; i++) {
            VitalSigns vitalSigns = new VitalSigns();
            vitalSigns.setTotalCholestrol(dataFactory.getNumberBetween(130, 330));
            vitalSigns.setHdlCholestrol(dataFactory.getNumberBetween(25, 75));
            if (dataFactory.getNumberBetween(0, 2) > 0) //means 1 means diabetic
            {
                vitalSigns.setIsDiabetic(true);
            } else {
                vitalSigns.setIsDiabetic(false);
            }
            if (dataFactory.getNumberBetween(0, 2) > 0) //means 1 means smoer
            {
                vitalSigns.setIsSmoker(true);
            } else {
                vitalSigns.setIsSmoker(false);
            }
            vitalSigns.setSystolicBloodPressure(dataFactory.getNumberBetween(100, 200));
            vitalSigns.setDiastolicBloodPressure(dataFactory.getNumberBetween(70, 120));
            Date minDate = dataFactory.getDate(2000, 1, 1);
            Date maxDate = new Date();
            vitalSigns.setVitalSignRecordedDate(dataFactory.getDateBetween(maxDate, minDate));
            vsh.getVitalSignHistory().add(vitalSigns);
            //System.out.println("Vita Sign: " + vitalSigns);
        }
        return vsh;
    }
}
