/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author schau
 */
public class Individual {
    private int age;
    private String gender;
    private float totalCholesterol;
    private float hdlCholesterol;
    private String isSmoker;
    private String isDiabetic;
    private int systolicBP;
    private String isTreatedForBP;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public float getTotalCholesterol() {
        return totalCholesterol;
    }

    public void setTotalCholesterol(float totalCholesterol) {
        this.totalCholesterol = totalCholesterol;
    }

    public float getHdlCholesterol() {
        return hdlCholesterol;
    }

    public void setHdlCholesterol(float hdlCholesterol) {
        this.hdlCholesterol = hdlCholesterol;
    }

    public String getIsSmoker() {
        return isSmoker;
    }

    public void setIsSmoker(String isSmoker) {
        this.isSmoker = isSmoker;
    }

    public String getIsDiabetic() {
        return isDiabetic;
    }

    public void setIsDiabetic(String isDiabetic) {
        this.isDiabetic = isDiabetic;
    }

    public int getSystolicBP() {
        return systolicBP;
    }

    public void setSystolicBP(int systolicBP) {
        this.systolicBP = systolicBP;
    }

    public String getIsTreatedForBP() {
        return isTreatedForBP;
    }

    public void setIsTreatedForBP(String isTreatedForBP) {
        this.isTreatedForBP = isTreatedForBP;
    }   
}
