/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Directory.FamilyDirectory;
import java.util.ArrayList;

/**
 *
 * @author Oza Sagar
 */
public class House {
    
    public String houseName;
    public int houseId;
    public int communityId;
    public int numberOfFamily;
    public ArrayList<Family> familyList;

    public int getNumberOfFamily() {
        return numberOfFamily;
    }

    public void setNumberOfFamily(int numberOfFamily) {
        this.numberOfFamily = numberOfFamily;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public int getCommunityId() {
        return communityId;
    }

    public void setCommunityId(int communityId) {
        this.communityId = communityId;
    }

    public ArrayList<Family> getFamilyList() {
        return familyList;
    }

    public void setFamilyList(ArrayList<Family> familyList) {
        this.familyList = familyList;
    }

    @Override
    public String toString() {
        return "House{" + "houseName=" + houseName + ", houseId=" + houseId + ", communityId=" + communityId + ", numberOfFamily=" + numberOfFamily + ", familyList=" + familyList + '}';
    }
}
