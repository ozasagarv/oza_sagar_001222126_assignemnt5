/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Oza Sagar
 */
public class Family 
{
    public int familyId;
    public String familyName;
    public ArrayList<Person> personInFamilyList;
    public int houseId;
    public int totalNumberOfMembersInFamily;

    public int getFamilyId() {
        return familyId;
    }

    public void setFamilyId(int familyId) {
        this.familyId = familyId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public ArrayList<Person> getPersonInFamilyList() {
        return personInFamilyList;
    }

    public void setPersonInFamilyList(ArrayList<Person> personInFamilyList) {
        this.personInFamilyList = personInFamilyList;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public int getTotalNumberOfMembersInFamily() {
        return totalNumberOfMembersInFamily;
    }

    public void setTotalNumberOfMembersInFamily(int totalNumberOfMembersInFamily) {
        this.totalNumberOfMembersInFamily = totalNumberOfMembersInFamily;
    }

    @Override
    public String toString() {
        return "Family{" + "familyId=" + familyId + ", familyName=" + familyName + ", personInFamilyList=" + personInFamilyList + ", houseId=" + houseId + ", totalNumberOfMembersInFamily=" + totalNumberOfMembersInFamily + '}';
    }

    
}
