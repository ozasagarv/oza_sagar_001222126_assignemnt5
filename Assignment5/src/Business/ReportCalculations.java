/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Directory.CommunityDirectory;
import Directory.FamilyDirectory;
import Directory.HouseDirectory;
import Directory.PersonDirectory;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Chetana
 */
public class ReportCalculations {

    private int totalRiskScoreMale = 0;
    private int totalRiskScoreFemale = 0;
    private int countLowRisk = 0;
    private int countMedRisk = 0;
    private int countHighRisk = 0;
    int sumRisk = 0;
    private House house;
    private HouseDirectory houseDirectory;
    private Person person;
    private PersonDirectory personDirectory;
    private Family family;
    private FamilyDirectory familyDirectory;
    private Community community;
    private CommunityDirectory communityDirectory;
    //  private VitalSigns vitalSigns;
    // private VitalSignHistory vitalSignHistory;
    int ageChoPoints = 0;
    int totalCHLPoints = 0;
    int hdlCHLPoints = 0;
    int systolicBPPoints = 0;
    int diabeticPoints = 0;
    int smokerPoints = 0;
    int totalPoints = 0;
    int riskScore = 0;
    int bloodPressureTreatmentPoints = 0;

    public ReportCalculations() {

        person = new Person();
        personDirectory = new PersonDirectory();
        family = new Family();
        familyDirectory = new FamilyDirectory();
        house = new House();
        houseDirectory = new HouseDirectory();
        community = new Community();
        communityDirectory = new CommunityDirectory();
        //    vitalSignHistory = new VitalSignHistory();
        //   vitalSigns = new VitalSigns();

    }

    public void personLevelRiskScore(PersonDirectory personDirectory) {
        int countLow = 0;
        int countMed = 0;
        int countHigh = 0;

        for (Person person : personDirectory.getPersonDirectory()) {

            if (person.getRiskScore() >= 0 && person.getRiskScore() < 8) {
                countLow++;
            }
            if (person.getRiskScore() >= 8 && person.getRiskScore() < 14) {
                countMed++;
            }
            if (person.getRiskScore() > 14) {
                countHigh++;
            }

        }
        System.out.println("Total no of person with low risk score ---> " + countLow);
        System.out.println("Total no of person with medium risk score ---> " + countMed);
        System.out.println("Total no of person with high risk score ---> " + countHigh);

    }

    public void familyLevelRiskScore(FamilyDirectory familyDirectory) {
        int count = 0;

        for (Family family : familyDirectory.getFamilyList()) {
            ArrayList<Person> familyList = family.getPersonInFamilyList();
            //  System.out.println("FamilyList: " + familyList);
            System.out.println(" FAMILY LEVEL REPORT FOR --->" + family.getFamilyName());
            for (Person person : familyList) {

                for (VitalSigns vitalSigns : person.getVitalSignHistory().getVitalSignHistory()) {
                    count = count + 1;

                    if (person.getFamilyId() == (family.getFamilyId())) {
                        System.out.println(" VITAL SIGNS REPORT FOR ---> " + person.getFirstName() + " " + person.getLastName());
                        System.out.println(" TOTAL CHOLESTEROL " + vitalSigns.getTotalCholestrol() + "\t"
                                + " HDL CHOLESTEROL " + vitalSigns.getHdlCholestrol() + "\t"
                                + " BLOOD PRESSURE " + vitalSigns.getSystolicBloodPressure() + " - " + vitalSigns.getDiastolicBloodPressure() + "\t"
                                + " RECORDED TIMESTAMP " + vitalSigns.getVitalSignRecordedDate() + "\t"
                                + " SMOKER " + vitalSigns.isIsSmoker() + "\t"
                                + " DIABETIC " + vitalSigns.isIsDiabetic() + "\n");

                        if (person.getGender().equalsIgnoreCase("Male")) {
                            riskScore = calculateMaleRiskScore(vitalSigns, person.getAge());
                            if (riskScore >= 0 && riskScore < 8) {
                                countLowRisk = countLowRisk + 1;
                                System.out.println("Risk score of person ---> " + person.getFirstName() + " " + person.getLastName() + "is:" + riskScore + " RISK LEVEL ---> LOW RISK OF CVD");
                            } else if (riskScore >= 8 && riskScore < 14) {
                                countMedRisk = countMedRisk + 1;
                                System.out.println("Risk score of person ---> " + person.getFirstName() + " " + person.getLastName() + "is:" + riskScore + " RISK LEVEL ---> MODERATE RISK OF CVD");
                            } else if (riskScore >= 14 && riskScore < 21) {
                                countHighRisk = countHighRisk + 1;
                                System.out.println("Risk score of person ---> " + person.getFirstName() + " " + person.getLastName() + "is:" + riskScore + " RISK LEVEL ---> HIGH RISK OF CONTRACTING CVDs");
                            }

                            totalRiskScoreMale += riskScore;
                            System.out.println("Risk score of person ---> " + person.getFirstName() + " " + person.getLastName() + "is:" + riskScore);
                        } else {
                            riskScore = calculateFemaleRiskScore(vitalSigns, person.getAge());
                            totalRiskScoreFemale += riskScore;
                            System.out.println("Risk score of person ---> " + person.getFirstName() + " " + person.getLastName() + "is:" + riskScore);
                        }
                    }
                }
                riskScore = (totalRiskScoreMale + totalRiskScoreFemale) / (count);

                //    (countLowRisk + countMedRisk + countHighRisk);
                System.out.println(" \n Average risk score " + riskScore + "\t TOTAL LOW RISK --->" + countLowRisk + "\t TOTAL NO OF PEOPLE WITH MED  RISK ---> " + countMedRisk + "\t TOTAL NO OF PEOPLE WITH HIGH RISK --->" + countHighRisk);
            }
        }

    }

    public void houseLevelRiskScore(HouseDirectory houseDirectory) {
        // System.out.println(" HOUSE LEVEL REPORT FOR ---> ");
        int count = 0;
        ArrayList averageRiskScoreOfHouse = new ArrayList();
        //      for ( House house : houseDirectory.getHouseDirectory())
        for (House house : houseDirectory.getHouseDirectory()) 
        {
            //System.out.println(" HOUSE LEVEL REPORT FOR insdide house ---> ");

            for (Family family : house.getFamilyList()) 
            {
                if (family.getHouseId() == house.getHouseId()) 
                {
                    //System.out.println(" HOUSE LEVEL REPORT FOR ---> " + house.getHouseName());

                    for (Person person : family.getPersonInFamilyList()) 
                    {
                        if (person.getFamilyId() == family.getFamilyId()) 
                        {
                            
                            riskScore = person.getRiskScore();
                            averageRiskScoreOfHouse.add(riskScore);

                        }
                    }
                    
                }
                count = count + family.getTotalNumberOfMembersInFamily();
                //System.out.println("Count: " + count);
                
                
            }
            for (int i = 0; i < averageRiskScoreOfHouse.size(); i++) 
                {
                    sumRisk = (int) averageRiskScoreOfHouse.get(i) + sumRisk;
                }
           averageRiskScoreOfHouse.removeAll(averageRiskScoreOfHouse);
            //System.out.println("Average Risk Score: " + averageRiskScoreOfHouse);
            //System.out.println("Count: " + count);
            //System.out.println("Risk: " + sumRisk);
            System.out.println("Average Risk Of House " + house.getHouseName() + ": " + (float)sumRisk / (count*5));
            count =0;
            sumRisk = 0;

        }
    }

    public void communityLevelRiskScore(PersonDirectory personDirectory) {

        //PersonDirectory personDirectory = (PersonDirectory)hashmap.get("PersonDirectory");
        //System.out.println("Person Direc:  " + personDirectory);
        int count = 0;
        for (Person person : personDirectory.getPersonDirectory()) {
                System.out.println(" VITAL SIGNS REPORT FOR ---> " + person.getFirstName() + " " + person.getLastName());
                for (VitalSigns vitalSigns : person.getVitalSignHistory().getVitalSignHistory()) 
                {
                    count = count + 1;

                    if(person.getFamilyId() != 0) 
                    {
                        
                        System.out.println(" TOTAL CHOLESTEROL " + vitalSigns.getTotalCholestrol() + "\t"
                                + " HDL CHOLESTEROL " + vitalSigns.getHdlCholestrol() + "\t"
                                + " BLOOD PRESSURE " + vitalSigns.getSystolicBloodPressure() + " - " + vitalSigns.getDiastolicBloodPressure() + "\t"
                                + " RECORDED TIMESTAMP " + vitalSigns.getVitalSignRecordedDate() + "\t"
                                + " SMOKER " + vitalSigns.isIsSmoker() + "\t"
                                + " DIABETIC " + vitalSigns.isIsDiabetic() + "\n" );

                        if (person.getGender().equalsIgnoreCase("Male")) 
                        {
                            riskScore = calculateMaleRiskScore(vitalSigns, person.getAge());
                            
                        } else {
                            riskScore = calculateFemaleRiskScore(vitalSigns, person.getAge());
                            
                        }
                    }
                }
                System.out.println("Risk score of person ---> " + person.getFirstName() + " " + person.getLastName() + "is:" + riskScore + "\n \n");
                System.out.println("--------------------------------------------------------------------------------------------------------------");

                //    (countLowRisk + countMedRisk + countHighRisk);
                //System.out.println(" \n Average risk score " + riskScore + "\t TOTAL LOW RISK --->" + countLowRisk + "\t TOTAL NO OF PEOPLE WITH MED  RISK ---> " + countMedRisk + "\t TOTAL NO OF PEOPLE WITH HIGH RISK --->" + countHighRisk);
            }
        
        
    }

    public void cityLevelRiskScore(PersonDirectory personDirectory) {
        //PersonDirectory personDirectory = (PersonDirectory)hashmap.get("PersonDirectory");
        //System.out.println("Person Direc:  " + personDirectory);
        int count = 0;
        for (Person person : personDirectory.getPersonDirectory()) {
                System.out.println(" VITAL SIGNS REPORT FOR ---> " + person.getFirstName() + " " + person.getLastName());
                for (VitalSigns vitalSigns : person.getVitalSignHistory().getVitalSignHistory()) 
                {
                    count = count + 1;

                    if(person.getFamilyId() != 0) 
                    {
                        
                        System.out.println(" TOTAL CHOLESTEROL " + vitalSigns.getTotalCholestrol() + "\t"
                                + " HDL CHOLESTEROL " + vitalSigns.getHdlCholestrol() + "\t"
                                + " BLOOD PRESSURE " + vitalSigns.getSystolicBloodPressure() + " - " + vitalSigns.getDiastolicBloodPressure() + "\t"
                                + " RECORDED TIMESTAMP " + vitalSigns.getVitalSignRecordedDate() + "\t"
                                + " SMOKER " + vitalSigns.isIsSmoker() + "\t"
                                + " DIABETIC " + vitalSigns.isIsDiabetic() + "\n" );

                        if (person.getGender().equalsIgnoreCase("Male")) 
                        {
                            riskScore = calculateMaleRiskScore(vitalSigns, person.getAge());
                            
                        } else {
                            riskScore = calculateFemaleRiskScore(vitalSigns, person.getAge());
                            
                        }
                    }
                }
                System.out.println("Risk score of person ---> " + person.getFirstName() + " " + person.getLastName() + "is:" + riskScore + "\n \n");
                System.out.println("--------------------------------------------------------------------------------------------------------------");

                //    (countLowRisk + countMedRisk + countHighRisk);
                //System.out.println(" \n Average risk score " + riskScore + "\t TOTAL LOW RISK --->" + countLowRisk + "\t TOTAL NO OF PEOPLE WITH MED  RISK ---> " + countMedRisk + "\t TOTAL NO OF PEOPLE WITH HIGH RISK --->" + countHighRisk);
            }
    }

    public int calculateMaleRiskScore(VitalSigns vitalSigns, int age) {

        if (age >= 30 && age <= 34) {
            ageChoPoints = - 1;

        } else if (age >= 35 && age <= 39) {
            ageChoPoints = 0;

        } else if (age >= 40 && age <= 44) {
            ageChoPoints = 1;

        } else if (age >= 45 && age <= 49) {
            ageChoPoints = 2;

        } else if (age >= 50 && age <= 54) {
            ageChoPoints = 3;

        } else if (age >= 55 && age <= 59) {
            ageChoPoints = 4;

        } else if (age >= 60 && age <= 64) {
            ageChoPoints = 5;

        } else if (age >= 65 && age <= 69) {
            ageChoPoints = 6;

        } else if (age >= 70 && age <= 74) {
            ageChoPoints = 7;

        }

        //  System.out.println("Points based on Age: " + ageChoPoints);
        if (vitalSigns.getTotalCholestrol() < 4.14) {
            totalCHLPoints = - 3;
        }
        if (vitalSigns.getTotalCholestrol() <= 5.17 && vitalSigns.getTotalCholestrol() >= 4.15) {
            totalCHLPoints = 0;
        }
        if (vitalSigns.getTotalCholestrol() >= 5.18 && vitalSigns.getTotalCholestrol() <= 6.21) {
            totalCHLPoints = 1;
        }
        if (vitalSigns.getTotalCholestrol() >= 6.22 && vitalSigns.getTotalCholestrol() <= 7.24) {
            totalCHLPoints = 2;
        }
        if (vitalSigns.getTotalCholestrol() >= 7.25) {
            totalCHLPoints = 3;
        }

        //   System.out.println("Points based on Total Cholesterol: " + totalCHLPoints);
        if (vitalSigns.getHdlCholestrol() < 0.9) {
            hdlCHLPoints = 2;
        }

        if (vitalSigns.getHdlCholestrol() >= 0.91 && vitalSigns.getHdlCholestrol() <= 1.16) {
            hdlCHLPoints = 1;
        }
        if (vitalSigns.getHdlCholestrol() >= 1.17 && vitalSigns.getHdlCholestrol() <= 1.29) {
            hdlCHLPoints = 0;
        }
        if (vitalSigns.getHdlCholestrol() >= 1.30 && vitalSigns.getHdlCholestrol() <= 1.55) {
            hdlCHLPoints = 0;
        }
        if (vitalSigns.getHdlCholestrol() >= 1.56) {
            hdlCHLPoints = -2;
        }

        //   System.out.println("Points based on HDL: " + hdlCHLPoints);
        if (vitalSigns.getSystolicBloodPressure() < 120) {
            systolicBPPoints = 0;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 120 && vitalSigns.getSystolicBloodPressure() <= 129) {
            systolicBPPoints = 0;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 130 && vitalSigns.getSystolicBloodPressure() <= 139) {
            systolicBPPoints = 1;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 140 && vitalSigns.getSystolicBloodPressure() <= 149) {
            systolicBPPoints = 2;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 160) {
            systolicBPPoints = 3;
        }
        //    System.out.println("Points based on Systolic BP: " + systolicBPPoints);

        if (vitalSigns.isIsDiabetic()) {
            diabeticPoints = 2;
        }
        if (!vitalSigns.isIsDiabetic()) {
            diabeticPoints = 0;
        }
        //    System.out.println("Points based on Diabetics: " + diabeticPoints);

        if (vitalSigns.isIsSmoker()) {
            smokerPoints = 2;
        }
        if (!vitalSigns.isIsSmoker()) {
            smokerPoints = 0;
        }
        //    System.out.println(" Points based on smoking habbit :" + smokerPoints);

        /* if(vitalSigns.isIsTreatedForHighBp()){
        bloodPressureTreatmentPoints = 2;
        }
        else{
        bloodPressureTreatmentPoints = 0;
        }
         */
        // Calculating Total Points
        totalPoints = ageChoPoints + totalCHLPoints + hdlCHLPoints + smokerPoints + diabeticPoints + systolicBPPoints;

        //    System.out.println("TOTAL CHL POINTS: " + totalPoints);
        //Determine CHD risk from point total
        if (totalPoints < -1) {
            riskScore = 2;
        }
        if (totalPoints == 0) {
            riskScore = riskScore + 3;
        }
        if (totalPoints == 1) {
            riskScore = 3;
        }
        if (totalPoints == 2) {
            riskScore = 4;
        }
        if (totalPoints == 3) {
            riskScore = 5;
        }
        if (totalPoints == 4) {
            riskScore = 7;
        }
        if (totalPoints == 5) {
            riskScore = 8;
        }
        if (totalPoints == 6) {
            riskScore = 10;
        }
        if (totalPoints == 7) {
            riskScore = 13;
        }
        if (totalPoints == 8) {
            riskScore = 16;
        }
        if (totalPoints == 9) {
            riskScore = 20;
        }
        if (totalPoints == 10) {
            riskScore = 25;
        }
        if (totalPoints == 11) {
            riskScore = 31;
        }
        if (totalPoints == 12) {
            riskScore = 37;
        }
        if (totalPoints == 13) {
            riskScore = 45;
        }
        if (totalPoints >= 14) {
            riskScore = 53;
        }

        return riskScore;
    }

    public int calculateFemaleRiskScore(VitalSigns vitalSigns, int age) {
        int ageChoPoints = 0;
        int totalCHLPoints = 0;
        int hdlCHLPoints = 0;
        int systolicBPPoints = 0;
        int diabeticPoints = 0;
        int smokerPoints = 0;
        int totalPoints = 0;
        int riskScore = 0;

        if (age >= 30 && age <= 34) {
            ageChoPoints = - 9;

        } else if (age >= 35 && age <= 39) {
            ageChoPoints = - 4;
        } else if (age >= 40 && age <= 44) {
            ageChoPoints = 0;
        } else if (age >= 45 && age <= 49) {
            ageChoPoints = 3;
        } else if (age >= 50 && age <= 54) {
            ageChoPoints = 6;
        } else if (age >= 55 && age <= 59) {
            ageChoPoints = 7;
        } else if (age >= 60 && age <= 64) {
            ageChoPoints = 8;
        } else if (age >= 65 && age <= 69) {
            ageChoPoints = 8;
        } else if (age >= 70 && age <= 74) {
            ageChoPoints = 8;
        }

        //    System.out.println("Points based on Age: " + ageChoPoints);
        if (vitalSigns.getTotalCholestrol() < 4.14) {
            totalCHLPoints = - 2;
        }
        if (vitalSigns.getTotalCholestrol() <= 5.17 && vitalSigns.getTotalCholestrol() >= 4.15) {
            totalCHLPoints = 0;
        }
        if (vitalSigns.getTotalCholestrol() >= 5.18 && vitalSigns.getTotalCholestrol() <= 6.21) {
            totalCHLPoints = 1;
        }
        if (vitalSigns.getTotalCholestrol() >= 6.22 && vitalSigns.getTotalCholestrol() <= 7.24) {
            totalCHLPoints = 1;
        }
        if (vitalSigns.getTotalCholestrol() >= 7.25) {
            totalCHLPoints = 3;
        }

        //    System.out.println("Points based on Total Cholesterol: " + totalCHLPoints);
        if (vitalSigns.getHdlCholestrol() < 0.9) {
            hdlCHLPoints = 5;
        }

        if (vitalSigns.getHdlCholestrol() >= 0.91 && vitalSigns.getHdlCholestrol() <= 1.16) {
            hdlCHLPoints = 2;
        }
        if (vitalSigns.getHdlCholestrol() >= 1.17 && vitalSigns.getHdlCholestrol() <= 1.29) {
            hdlCHLPoints = 1;
        }
        if (vitalSigns.getHdlCholestrol() >= 1.30 && vitalSigns.getHdlCholestrol() <= 1.55) {
            hdlCHLPoints = 0;
        }
        if (vitalSigns.getHdlCholestrol() >= 1.56) {
            hdlCHLPoints = - 3;
        }

        //    System.out.println("Points based on HDL Cholesterol: " + hdlCHLPoints);
        if (vitalSigns.getSystolicBloodPressure() < 120) {
            systolicBPPoints = - 3;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 120 && vitalSigns.getSystolicBloodPressure() <= 129) {
            systolicBPPoints = 0;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 130 && vitalSigns.getSystolicBloodPressure() <= 139) {
            systolicBPPoints = 0;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 140 && vitalSigns.getSystolicBloodPressure() <= 149) {
            systolicBPPoints = 2;
        }
        if (vitalSigns.getSystolicBloodPressure() >= 160) {
            systolicBPPoints = 3;
        }

        //    System.out.println("Points based on Systolic BP: " + systolicBPPoints);
        if (vitalSigns.isIsDiabetic()) {
            diabeticPoints = 4;
        } else if (!vitalSigns.isIsDiabetic()) {
            diabeticPoints = 0;
        }

        // System.out.println("Points based on Diabetics: " + diabeticPoints);
        if (vitalSigns.isIsSmoker()) {
            smokerPoints = 2;
        } else if (!vitalSigns.isIsSmoker()) {
            smokerPoints = 0;
        }
        //  System.out.println("Points based on Smoking Habit: " + smokerPoints);

        // Calculating Total Points
        totalPoints = ageChoPoints + totalCHLPoints + hdlCHLPoints + smokerPoints + diabeticPoints + systolicBPPoints;

        //    System.out.println("TOTAL POINTS:" + totalPoints);
        //Determine CHD risk from point total
        if (totalPoints < -2) {
            riskScore = 1;
        }
        if (totalPoints == -1) {
            riskScore = 2;
        }
        if (totalPoints == 0) {
            riskScore = 2;
        }
        if (totalPoints == 1) {
            riskScore = 2;
        }
        if (totalPoints == 2) {
            riskScore = 3;
        }
        if (totalPoints == 3) {
            riskScore = 3;
        }
        if (totalPoints == 4) {
            riskScore = 4;
        }
        if (totalPoints == 5) {
            riskScore = 4;
        }
        if (totalPoints == 6) {
            riskScore = 5;
        }
        if (totalPoints == 7) {
            riskScore = 6;
        }
        if (totalPoints == 8) {
            riskScore = 7;
        }
        if (totalPoints == 9) {
            riskScore = 8;
        }
        if (totalPoints == 10) {
            riskScore = 10;
        }
        if (totalPoints == 11) {
            riskScore = 11;
        }
        if (totalPoints == 12) {
            riskScore = 13;
        }
        if (totalPoints == 13) {
            riskScore = 15;
        }
        if (totalPoints == 14) {
            riskScore = 53;
        }
        if (totalPoints == 15) {
            riskScore = 18;
        }
        if (totalPoints == 16) {
            riskScore = 24;
        }
        if (totalPoints >= 17) {
            riskScore = 27;
        }
        return riskScore;
    }

}
