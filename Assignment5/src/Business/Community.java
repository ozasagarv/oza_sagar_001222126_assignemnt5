/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Directory.HouseDirectory;
import java.util.ArrayList;

/**
 *
 * @author Oza Sagar
 */
public class Community 
{
    public int communityId;
    public String communityName;
    public HouseDirectory houseDirectory;

    public int getCommunityId() {
        return communityId;
    }

    public void setCommunityId(int communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public HouseDirectory getHouseDirectory() {
        return houseDirectory;
    }

    public void setHouseDirectory(HouseDirectory houseDirectory) {
        this.houseDirectory = houseDirectory;
    }

    @Override
    public String toString() {
        return "Community{" + "communityId=" + communityId + ", communityName=" + communityName + ", houseDirectory=" + houseDirectory + '}';
    }
    
    
}
