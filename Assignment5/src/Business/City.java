/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Directory.CommunityDirectory;
import Directory.FamilyDirectory;
import java.util.ArrayList;

/**
 *
 * @author Oza Sagar
 */
public class City 
{
    public int cityId = 1;
    public CommunityDirectory communityDirectory;
    public String cityName = "Boston";
    public FamilyDirectory familyDirectory;

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public CommunityDirectory getCommunityDirectory() {
        return communityDirectory;
    }

    public void setCommunityDirectory(CommunityDirectory communityDirectory) {
        this.communityDirectory = communityDirectory;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public FamilyDirectory getFamilyDirectory() {
        return familyDirectory;
    }

    public void setFamilyDirectory(FamilyDirectory familyDirectory) {
        this.familyDirectory = familyDirectory;
    }

    @Override
    public String toString() {
        return "City{" + "cityId=" + cityId + ", communityDirectory=" + communityDirectory + ", cityName=" + cityName + ", familyDirectory=" + familyDirectory + '}';
    }
    
}
