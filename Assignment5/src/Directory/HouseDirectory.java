/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Directory;

import Business.House;
import java.util.ArrayList;

/**
 *
 * @author Oza Sagar
 */
public class HouseDirectory 
{
    ArrayList<House> houseDirectory;
    public HouseDirectory()
    {
        houseDirectory = new ArrayList<House>();
    }

    public ArrayList<House> getHouseDirectory() {
        return houseDirectory;
    }

    public void setHouseDirectory(ArrayList<House> houseDirectory) {
        this.houseDirectory = houseDirectory;
    }

    @Override
    public String toString() {
        return "HouseDirectory{" + "houseDirectory=" + houseDirectory + '}';
    }
}
