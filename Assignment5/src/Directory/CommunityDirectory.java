/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Directory;

import Business.Community;
import java.util.ArrayList;

/**
 *
 * @author Oza Sagar
 */
public class CommunityDirectory 
{
    ArrayList<Community> communityList;
    public CommunityDirectory() {
        
        communityList = new ArrayList<Community>();        
    }

    public ArrayList<Community> getCommunityList() {
        return communityList;
    }

    public void setCommunityList(ArrayList<Community> communityList) {
        this.communityList = communityList;
    }

    @Override
    public String toString() {
        return "CommunityDirectory{" + "communityList=" + communityList + '}';
    }
    
}
