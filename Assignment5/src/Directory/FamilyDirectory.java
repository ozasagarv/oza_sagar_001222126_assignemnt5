/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Directory;

import Business.Family;
import java.util.ArrayList;

/**
 *
 * @author Oza Sagar
 */
public class FamilyDirectory 
{
    private ArrayList<Family> familyList;

    public FamilyDirectory() {
        
        familyList = new ArrayList<Family>();
    }

    public ArrayList<Family> getFamilyList() {
        return familyList;
    }

    public void setFamilyList(ArrayList<Family> familyList) {
        this.familyList = familyList;
    }

    @Override
    public String toString() {
        return "FamilyDirectory{" + "familyList=" + familyList + '}';
    }
}
