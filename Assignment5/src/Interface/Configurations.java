/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Adult;
import Business.Calculation;
import Business.City;
import Business.Individual;
import Business.Initialization;
import Business.Person;
import Business.ReportCalculations;
import Directory.PersonDirectory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import org.fluttercode.datafactory.impl.DataFactory;

/**
 *
 * @author Oza Sagar
 */
public class Configurations {   // private ReportCalculations reportCalculations;

    public static void main(String args[]) throws IOException {
        Initialization init = new Initialization();
        HashMap hashMap = init.initializePersonDirectory();
        City city = (City)hashMap.get("city");
        ReportCalculations reportCalculations = new ReportCalculations();
      
       reportCalculations.personLevelRiskScore((PersonDirectory)hashMap.get("PersonDirectory"));
        int age;
        int genderChosen;
        float totalCholesterol;
        float hdlCholesterol;
        int smoker;
        int diabetic;
        int systolicBP;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Individual individual = new Individual();
        Calculation calculate = new Calculation();

        System.out.println("**************RISK SCORE REPORT**************");
        System.out.println("Select one of the option below:");
        System.out.println("1.CALCULATE  YOUR RISK SCORE");
        System.out.println("2.VIEW REPORTS AT VARIOUS LEVELS");

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Please enter your choice");
        int choice = reader.nextInt(); // Scans the next token of the input as an int.
        switch (choice) {
            case 1:
                //Retrieving the values from the Initialization file
//                double gpaOfStudent = Initialization.initializeStudent().getTranscript().calculateGPA();

                System.out.println("-----------------------PLEASE ENTER YOUR DETAILS-----------------------");

                //Age Input 
                do {
                    System.out.println("AGE:");
                    age = Integer.parseInt(br.readLine());
                    if (age > 30) {

                        individual.setAge(age);
                    } else {

                        System.out.println("Age should be greater than or equal to 30!");
                    }
                } while (age < 30);

                // Gender Input
                System.out.println("CHOSE YOU GENDER-->1.Male      2.Female");
                do {
                    genderChosen = reader.nextInt();
                    if (genderChosen == 1) {
                        individual.setGender("Male");
                        break;
                    } else if (genderChosen == 2) {
                        individual.setGender("Female");
                        break;
                    } else {
                        System.out.println("Select either 1 for Male 2 for Female");
                    }
                } while (genderChosen == 1 || genderChosen == 2);

                // Total Cholesterol input
                System.out.println("TOTAL CHOLESTEROL:");
                do {
                    totalCholesterol = Float.parseFloat(br.readLine());

                    if (totalCholesterol >= 3.6 && totalCholesterol <= 10.3) {
                        individual.setTotalCholesterol(totalCholesterol);
                    } else {
                        System.out.println("Total Cholesterol should be between 3.6 and 10.3 ");
                    }
                } while (totalCholesterol < 3.6 || totalCholesterol > 10.3);

// HDL input
                System.out.println("HDL CHOLESTEROL:");

                do {
                    hdlCholesterol = Float.parseFloat(br.readLine());

                    if (hdlCholesterol >= 0.8 && hdlCholesterol <= 4.0) {
                        individual.setHdlCholesterol(hdlCholesterol);
                        break;
                    } else {
                        System.out.println("HDL Cholesterol should be between 0.8 and 4.0 ");
                    }
                } while (hdlCholesterol < 0.8 || hdlCholesterol > 4.0);

// Smoker input
                do {
                    System.out.println("SMOKER:--> 1.YES        2.NO");
                    smoker = Integer.parseInt(br.readLine());
                    if (smoker == 1) {
                        individual.setIsSmoker("YES");
                        break;
                    } else if (smoker == 2) {
                        individual.setIsSmoker("NO");
                        break;
                    } else {
                        System.out.println("Select 1 for YES or 2 for NO");
                    }
                } while (smoker != 1 || smoker != 2);

// Diabetic input              
                do {
                    System.out.println("DIABETIC:--> 1.YES        2.NO");
                    diabetic = Integer.parseInt(br.readLine());
                    if (diabetic == 1) {
                        individual.setIsDiabetic("YES");
                        break;
                    } else if (diabetic == 2) {
                        individual.setIsDiabetic("NO");
                        break;
                    } else {
                        System.out.println("Select 1 for YES or 2 for NO");
                    }
                } while (diabetic != 1 || diabetic != 2);

//Systolic BP           
                do {
                    System.out.println("SYSTOLIC BP: ");
                    systolicBP = Integer.parseInt(br.readLine());
                    if (systolicBP >= 100 && systolicBP <= 200) {
                        individual.setSystolicBP(systolicBP);
                        break;
                    } else {
                        System.out.println("Systolic BP should be between 100 and 200");
                    }
                } while (systolicBP < 100 || systolicBP > 200);

//Displaying Calculated risk
                if (individual.getGender().equals("Male")) {
                    System.out.println("YOUR RISK SCORE IS: " + calculate.calculateMaleRiskScore(individual) + "%");
                } else {
                    System.out.println("YOUR RISK SCORE IS: " + calculate.calculateFemaleRiskScore(individual) + "%");
                }
                break;
            case 2:
                
                System.out.println("******************Reports*************************");

                System.out.println("*********Welcome to the CITY LEVEL ECOSYSTEM*************** ");
                System.out.println("*********Please enter the choice below for Reports*************** ");
                System.out.println("1.CITY LEVEL REPORTS");
                System.out.println("2.COMMUNITY LEVEL REPORTS");
                System.out.println("3.HOUSE LEVEL REPORTS");
                System.out.println("4.FAMILY LEVEL REPORTS");
                System.out.println("5.PERSON LEVEL REPORTS");
                
                int subChoice = reader.nextInt();
                        switch(subChoice)
                    {
                        case 1 : System.out.println("***********************************City Level Reports: *****************************************");
                                 reportCalculations.cityLevelRiskScore((PersonDirectory)hashMap.get("PersonDirectory"));
                                 break;
                                 
                        case 2 : System.out.println("***********************************Community Level Reports: *****************************************");
                                 reportCalculations.communityLevelRiskScore((PersonDirectory)hashMap.get("PersonDirectory"));
                                 break;
                                 
                        case 3 : System.out.println("***********************************House Level Reports: *****************************************");
                                 reportCalculations.houseLevelRiskScore(city.getCommunityDirectory().getCommunityList().get(0).getHouseDirectory());
                                 break;
                        
                        case 4 : System.out.println("***********************************Family Level Reports: *****************************************");
                                 reportCalculations.familyLevelRiskScore(city.getFamilyDirectory());
                                 break;
                         
                        case 5 : System.out.println("***********************************Personal Level Reports: *****************************************");
                                 reportCalculations.personLevelRiskScore((PersonDirectory)hashMap.get("PersonDirectory"));
                                 break;
                                 
                        default : System.out.println("Please enter a valid number!!!");
                                  break;
                    }

            default:
                //System.out.println("Please enter a valid number!!!");
                break;
        }

    }

}
